package de.imedia24.shop

import de.imedia24.shop.controller.ProductController
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

/**
 * Unit Test For ProductController
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductControllerTest(@Autowired
                             var productController: ProductController){

    @Test
    fun testHelloController() {
        val product = productController.findProductsBySku("123")
        Assertions.assertNotNull(product)
        Assertions.assertEquals(product.body?.description, "Red shirt")
    }
}