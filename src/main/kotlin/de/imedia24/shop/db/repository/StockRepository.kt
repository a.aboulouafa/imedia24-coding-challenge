package de.imedia24.shop.db.repository

import de.imedia24.shop.db.entity.StockEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

/**
 * @author a.abouelouafa
 */

@Repository
interface StockRepository : CrudRepository<StockEntity, String> {
    @Transactional
    fun findById(id: Long?): StockEntity?
}