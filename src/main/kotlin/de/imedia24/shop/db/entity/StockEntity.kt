package de.imedia24.shop.db.entity

import org.springframework.lang.NonNull
import javax.persistence.*

@Entity
@Table(name = "stock")
data class StockEntity (
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    var id: Long,

    @Column(name = "name", nullable = false)
    val name: String,

    @Column(name = "description")
    val description: String? = null,
) {
    constructor() : this(1L, "Casablanca Stock", "Stock located near to casa")
}