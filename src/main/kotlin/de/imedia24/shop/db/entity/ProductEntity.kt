package de.imedia24.shop.db.entity

import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.persistence.*


@Entity
@Table(name = "products")
data class ProductEntity(
    @Id
    @Column(name = "sku", nullable = false)
    val sku: String,

    @Column(name = "name", nullable = false)
    val name: String,

    @Column(name = "description")
    val description: String? = "",

    @Column(name = "price", nullable = false)
    val price: BigDecimal,

    @UpdateTimestamp
    @Column(name = "created_at", nullable = false)
    val createdAt: ZonedDateTime,

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    val updatedAt: ZonedDateTime,

    @ManyToOne(fetch = FetchType.EAGER, cascade = arrayOf(CascadeType.ALL))
    @JoinColumn(name = "stock_id")
    val stock:StockEntity
) {
    constructor(): this("1", "", "", BigDecimal(1), ZonedDateTime.now(ZoneId.of("UTC")), ZonedDateTime.now(ZoneId.of("UTC")), StockEntity())
    constructor(sku: String, name: String, description: String, price: BigDecimal) :
            this(sku, name, description, price, ZonedDateTime.now(ZoneId.of("UTC")), ZonedDateTime.now(ZoneId.of("UTC")), StockEntity())
}
