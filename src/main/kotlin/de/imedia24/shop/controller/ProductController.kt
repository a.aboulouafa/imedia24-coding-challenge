package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.data.repository.query.Param
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if (product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    /**
     * Endpoint to get a list of product details by list of SKUs: /products?skus=123,4567,8901,2345,67789
     * @param skus
     * @return ResponseEntity<MutableList<ProductResponse?>>
     */
    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsByListSku(
        @Param("skus") skus: String
    ): ResponseEntity<MutableList<ProductResponse?>> {
        logger.info("Request for product list $skus")

        val regex = "\\s*,\\s*"
        val skuList: List<String> = skus.split(",").toList()
        logger.info("Split skuList $skuList")
        var productsList = mutableListOf<ProductResponse?>()
        for (sk in skuList) {
            val product: ProductResponse? = productService.findProductBySku(sk)
            productsList.add(product)
        }
        return if (productsList == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(productsList)
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addNewProduct(
        @RequestBody product: ProductEntity
    ): ResponseEntity<ProductResponse> {
        logger.info("addNewProduct product $product")

        val prodEntity = productService.createProduct(product)
        return if (prodEntity == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(prodEntity.toProductResponse())
        }
    }

    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun updateProduct(
        @RequestBody p: ProductEntity
    ): ResponseEntity<ProductResponse> {
        logger.info("updateProduct product $p")

        val productEntity = productService.updateProductBySku(p.sku, p)
        return if (productEntity == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(productEntity.toProductResponse())
        }
    }
}
