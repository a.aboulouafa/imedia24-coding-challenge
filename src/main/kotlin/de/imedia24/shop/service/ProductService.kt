package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.exception.ProductNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    fun createProduct(product: ProductEntity): ProductEntity = productRepository.save(product)
    fun updateProductBySku(sku: String, p: ProductEntity): ProductEntity {
        return if (productRepository.existsById(sku)) {
            productRepository.save(
                ProductEntity(
                    sku = p.sku,
                    name = p.name,
                    description = p.description ?: "",
                    price = p.price,
                    createdAt = p.createdAt,
                    updatedAt = p.updatedAt,
                    stock = p.stock
                )
            )
        } else throw ProductNotFoundException(HttpStatus.NOT_FOUND, "No matching product was found")
    }
}
