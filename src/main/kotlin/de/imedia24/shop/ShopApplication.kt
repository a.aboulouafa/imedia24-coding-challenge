package de.imedia24.shop

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.StockEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.db.repository.StockRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import java.math.BigDecimal
import java.time.ZoneId
import java.time.ZonedDateTime

@SpringBootApplication
class ShopApplication {

    @Bean
    fun init(repository: ProductRepository, stockRepository: StockRepository) = CommandLineRunner {
        val zonedTime = ZonedDateTime.now(ZoneId.of("UTC"))
        val stock: StockEntity? =
            stockRepository.save(StockEntity(1L, "Casablanca Stock", "Stock located near to casa"))
        if (stock != null) {
            repository.save(ProductEntity("123", "Shirt", "Red shirt", BigDecimal(120), zonedTime, zonedTime, stock))
            repository.save(
                ProductEntity(
                    "4567",
                    "Sweater",
                    "Blue sweater",
                    BigDecimal(300),
                    zonedTime,
                    zonedTime,
                    stock
                )
            )
            repository.save(
                ProductEntity(
                    "8901",
                    "Shoes",
                    "Black Shoes 42",
                    BigDecimal(500),
                    zonedTime,
                    zonedTime,
                    stock
                )
            )
            repository.save(ProductEntity("2345", "Vest", "Green Vest", BigDecimal(700), zonedTime, zonedTime, stock))
            repository.save(
                ProductEntity(
                    "67789",
                    "Umbrella",
                    "Pink Umbrella",
                    BigDecimal(136),
                    zonedTime,
                    zonedTime,
                    stock
                )
            )
        }
    }
}

fun main(args: Array<String>) {
    runApplication<ShopApplication>(*args)
}
