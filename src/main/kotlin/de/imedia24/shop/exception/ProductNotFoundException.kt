package de.imedia24.shop.exception

import org.springframework.http.HttpStatus

class ProductNotFoundException(val statusCode: HttpStatus, val reason: String) : Exception()